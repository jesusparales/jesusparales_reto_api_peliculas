require 'active_record'
require 'rest-client'
#require_relative '../db/connection'
require_relative 'movies/movie'


class CreateHtml

	def generate_html

		movie = Movie.all

		File.open("html/movies.html", "w+") do |f|
			f.write("<html>\n")
			f.write("<head>\n<title>OMDBAPI</title>\n</head>\n")
			f.write('<link rel="stylesheet" href="style.css"')
			f.write("\n<body>\n")
			f.write("<div>\n")
			f.write('<h1 align="center">OMDb</h1>')
			f.write("\n</div>\n")
			f.write('<div class="square">')
			movie.each do |m|
				f.write('<div class="item">')
				f.write("<div>\n")
				f.write("<img src='#{m.poster}'>")
				f.write("</div>\n")
				f.write("<div>\n")
				f.write("<ul>\n")
				f.write("<li><b>Titulo: </b>#{m.title}</li>\n")
				f.write("<li><b>Publicacion: </b>#{m.released}</li>\n")
				f.write("<li><b>Sinopsis(s): </b>#{m.plot}</li>\n")
				f.write("<li><b>Website: </b>#{m.website}</li>\n")
				f.write("<li><b>Metascore: </b>#{m.metascore}</li>\n")
				f.write("<li><b>Raiting: </b>#{m.raiting}</li>\n")
				f.write("</ul>\n")
				f.write("</div>\n")
				f.write("</div>\n")
			end
			f.write("</div>\n")
			f.write("</body>\n</html>\n")
		end

		p "Archivo Generado Exitosamente!"
	end

end
