require 'rest-client'
require 'json'

class GetMovie
		attr_accessor :url, :data, :apikey, :title

	def initialize
		#@title = gets.chomp
		@title = "the lord of the rings"
		@apikey = 'c8e51eb5'
		#@url = "http://www.omdbapi.com/?t=terminator&apikey=#{@apikey}"
		@url = "http://www.omdbapi.com/?t=#{@title}&apikey=#{@apikey}"
	end

	def get_data
		RestClient.get(@url)
	end

	def parse_data
		JSON.parse get_data
	end

	def show_data
		@data = parse_data
		@data.each do |k| 
			puts k
		end	
	end

	def find_movie		
		show_data		
	end

end

#print "Ingrese Película a Buscar: "
#GetMovie.new.find_movie
#p GetMovie.new.parse_data