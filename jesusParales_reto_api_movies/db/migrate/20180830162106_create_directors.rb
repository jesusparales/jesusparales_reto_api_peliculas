class CreateDirectors < ActiveRecord::Migration[5.2]
  def change
    create_table :directors do |t|
      t.string :name
      t.references :movie_directors, foreign_key: true
    end
  end
end
