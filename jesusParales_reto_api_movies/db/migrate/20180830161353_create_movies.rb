class CreateMovies < ActiveRecord::Migration[5.2]
  def change
    create_table :movies do |t|
      t.string :title
      t.string :plot
      t.string :released
      t.string :website
      t.string :poster
      t.string :metascore
      t.string :raiting
      t.references :production, foreign_key: true
    end
  end
end
