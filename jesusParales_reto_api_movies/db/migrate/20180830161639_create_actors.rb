class CreateActors < ActiveRecord::Migration[5.2]
  def change
    create_table :actors do |t|
      t.string :name
      t.references :movie_actors, foreign_key: true
    end
  end
end
