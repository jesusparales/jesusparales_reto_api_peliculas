# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_08_30_162557) do

  create_table "actors", force: :cascade do |t|
    t.string "name"
    t.integer "movie_actors_id"
    t.index ["movie_actors_id"], name: "index_actors_on_movie_actors_id"
  end

  create_table "directors", force: :cascade do |t|
    t.string "name"
    t.integer "movie_directors_id"
    t.index ["movie_directors_id"], name: "index_directors_on_movie_directors_id"
  end

  create_table "genres", force: :cascade do |t|
    t.string "genre"
    t.integer "movie_genres_id"
    t.index ["movie_genres_id"], name: "index_genres_on_movie_genres_id"
  end

  create_table "languages", force: :cascade do |t|
    t.string "language"
    t.integer "movie_languages_id"
    t.index ["movie_languages_id"], name: "index_languages_on_movie_languages_id"
  end

  create_table "movie_actors", force: :cascade do |t|
    t.integer "movie_id"
    t.integer "actor_id"
    t.index ["actor_id"], name: "index_movie_actors_on_actor_id"
    t.index ["movie_id"], name: "index_movie_actors_on_movie_id"
  end

  create_table "movie_directors", force: :cascade do |t|
    t.integer "movie_id"
    t.integer "director_id"
    t.index ["director_id"], name: "index_movie_directors_on_director_id"
    t.index ["movie_id"], name: "index_movie_directors_on_movie_id"
  end

  create_table "movie_genres", force: :cascade do |t|
    t.integer "movie_id"
    t.integer "genre_id"
    t.index ["genre_id"], name: "index_movie_genres_on_genre_id"
    t.index ["movie_id"], name: "index_movie_genres_on_movie_id"
  end

  create_table "movie_languages", force: :cascade do |t|
    t.integer "movie_id"
    t.integer "language_id"
    t.index ["language_id"], name: "index_movie_languages_on_language_id"
    t.index ["movie_id"], name: "index_movie_languages_on_movie_id"
  end

  create_table "movies", force: :cascade do |t|
    t.string "title"
    t.string "plot"
    t.string "released"
    t.string "website"
    t.string "poster"
    t.string "metascore"
    t.string "raiting"
    t.integer "production_id"
    t.index ["production_id"], name: "index_movies_on_production_id"
  end

  create_table "productions", force: :cascade do |t|
    t.string "name"
    t.integer "movie_id"
    t.index ["movie_id"], name: "index_productions_on_movie_id"
  end

end
