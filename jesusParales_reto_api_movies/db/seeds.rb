require_relative '../lib/test'
require 'faker'

Dir[File.join('db', 'seeds', '*.rb')].sort.each do |seed|
  puts "Loading seed | #{File.basename(seed)}"
  load seed
end
#Country.destroy_all


=begin 

Country.create!([{
  name: "Ant-Man",
  director: "Peyton Reed",
  storyline: "Armed with the astonishing ability to shrink in scale but increase in strength, con-man Scott Lang must embrace his inner-hero and help his mentor, Dr. Hank Pym, protect the secret behind his spectacular Ant-Man suit from a new generation of towering threats. Against seemingly insurmountable obstacles, Pym and Lang must plan and pull off a heist that will save the world.",
  watched_on: 5.days.ago
},
{
  title: "Pixels",
  director: "Chris Columbus",
  storyline: "When aliens misinterpret video feeds of classic arcade games as a declaration of war, they attack the Earth in the form of the video games.",
  watched_on: 3.days.ago
},
{
  title: "Terminator Genisys",
  director: "Alan Taylor",
  storyline: "When John Connor, leader of the human resistance, sends Sgt. Kyle Reese back to 1984 to protect Sarah Connor and safeguard the future, an unexpected turn of events creates a fractured timeline. Now, Sgt. Reese finds himself in a new and unfamiliar version of the past, where he is faced with unlikely allies, including the Guardian, dangerous new enemies, and an unexpected new mission: To reset the future...",
  watched_on: 10.days.ago
}])

20.times do |index|
  Country.create!(name: Faker::Address.country)
end

Country.create!([{
  name: 'England'
}])

10.times do |index|
  Invoice.create!(date: Faker::Time.between(4.month.ago, 1.week.ago)
                  is_paid: rand[true,false]
                  customer_id: rand[14])
end

10.times do |index|
  Customer.create!(name: Faker::Name.name,
                  address: Faker::Address.street_address,
                  phone: Faker::PhoneNumber.phone_number,
                  paid: Faker::Number.decimal(3,2),
                  debt: Faker::Number.decimal(3,2),
                  country_id: rand(15)+1)
end

10.times do |index|
  bool = [true, false]
  Invoice.create!(date: Faker::Time.between(4.month.ago, 1.week.ago),
                  is_paid: bool.sample,
                  customer_id: rand(16)+1)
end

10.times do |index|
  Publisher.create!(name: Faker::Company.name,
               country_id: rand(15)+1)
end

10.times do |index|
  Book.create!(title: Faker::Job.title,
               author: Faker::App.author,
               price: Faker::Number.decimal(3,2),
               publisher_id: rand(15)+1)
end

15.times do |index|
  Detail.create!(quantity: rand(10)+1,
               book_id: rand(17)+1,
               invoice_id: rand(15)+1,
               price: Faker::Number.decimal(3,2))
end

=end
 
#p "Created #{Detail.count} entries"