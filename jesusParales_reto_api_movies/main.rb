require 'active_record'
require_relative 'db/connection'
require_relative 'lib/movies/actor'
require_relative 'lib/movies/director'
require_relative 'lib/movies/genre'
require_relative 'lib/movies/language'
require_relative 'lib/movies/movie'
require_relative 'lib/movies/production'
require_relative 'lib/generate_html'

class Main

	def fill_db

		Actor.create_actors
		Director.create_directors
		Genre.create_genres
		Language.create_languages
		Movie.create_movies
		Production.create_productions

	end

	def get_html
		CreateHtml.new.generate_html
	end

end

#Main.new.fill_db
Main.new.get_html
